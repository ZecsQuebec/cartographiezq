-- LACS
drop table if exists carto.temp;
drop table if exists carto.tr;
drop table if exists carto.copie_table;
drop table if exists carto.copie_lac;


-- JOIN 1 : LAC + ESPECES INDÉSIRABLES
-- changer les codes numériques en leurs valeurs texte
create table corpo.temp as select * from corpo.lac_sp_indesi;
alter table corpo.temp add column newcol text;
update corpo.temp 
	set newcol = corpo.sp_indesi.sp_indesi
    from corpo.sp_indesi
    where corpo.temp.sp_ind_id = corpo.sp_indesi.id_sp_ind;

-- combiner les valeurs par sifz unique (garder une seule ligne par id)
create table corpo.tr 
	as select lac_id, array_to_string(array_agg(newcol order by newcol),',') as newcol 
	from corpo.temp 
    group by lac_id;
delete from corpo.tr where lac_id is null;

-- créer une nouvelle table dans le schema CARTO avec toutes les données de lac + la nouvelle colonne 
create table carto.copie_table (like corpo.lac excluding constraints); -- les constraintes sont enlevées parce que sinon Esri écrase le ID quand la variable est un  pkey et assigne des nouveaux ID
insert into carto.copie_table select * from corpo.lac;
alter table carto.copie_table add column sp_indesi text;
update carto.copie_table	
	set sp_indesi = corpo.tr.newcol
	from corpo.tr
	where carto.copie_table.id_lac = corpo.tr.lac_id;

-- effacer les deux tables temporaires qui ne serviront plus
drop table corpo.temp, corpo.tr;


-- JOIN 2 : LAC + ESPECES DE POISSON
create table corpo.temp as select * from corpo.lac_sp_poisson; 
alter table corpo.temp add column newcol text;
update corpo.temp 
	set newcol = corpo.espece_poisson.sp_poisson
    from corpo.espece_poisson
    where corpo.temp.sp_pois_id = corpo.espece_poisson.id_sp_pois;
    
create table corpo.tr 
	as select lac_id, array_to_string(array_agg(newcol order by newcol),',') as newcol
	from corpo.temp 
    group by lac_id;
delete from corpo.tr where lac_id is null; 

alter table carto.copie_table add column sp_poisson text;
update carto.copie_table
	set sp_poisson = corpo.tr.newcol
	from corpo.tr
    where carto.copie_table.id_lac = corpo.tr.lac_id;

drop table corpo.temp, corpo.tr;


-- JOIN 3 : LAC + TYPE D'INTÉRET
create table corpo.temp as select * from corpo.lac_type_lac_int; 
alter table corpo.temp add column newcol text;
update corpo.temp 
	set newcol = corpo.type_lac_int.type_inte
    from corpo.type_lac_int
    where corpo.temp.typ_int_id = corpo.type_lac_int.id_typ_int;
    
create table corpo.tr 
	as select lac_id, array_to_string(array_agg(newcol order by newcol),',') as newcol
	from corpo.temp 
    group by lac_id;
delete from corpo.tr where lac_id is null; 

alter table carto.copie_table add column type_inte text;
update carto.copie_table
	set type_inte = corpo.tr.newcol
	from corpo.tr
    where carto.copie_table.id_lac = corpo.tr.lac_id;

drop table corpo.temp, corpo.tr;

-- Après le dernier join, enlever les SIFZ qui n'existent plus  et changer le nom de la nouvelle table créée
delete from carto.copie_table where delete_flag = 1;
alter table carto.copie_table rename to copie_lac;