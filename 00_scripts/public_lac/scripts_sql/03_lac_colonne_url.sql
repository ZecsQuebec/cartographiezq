---ajouter une colonne url pour référez au site Web dans le popup

--lac
alter table carto.copie_lac add column "url" text;
update carto.copie_lac set "url" = 'https://zec'||lower("nom_zec")||'.reseauzec.com/statuts-des-lacs';
update carto.copie_lac set "url" =replace("url", '-', '');
update carto.copie_lac set "url" = replace("url", ' ', '');
update carto.copie_lac set "url" = replace("url", 'é', 'e');
update carto.copie_lac set "url" = replace("url", 'è', 'e');
update carto.copie_lac set "url" = replace("url", 'statutsdeslacs', 'statuts-des-lacs');
