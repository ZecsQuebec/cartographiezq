@ECHO OFF
call 00_scripts\public_zec\scripts_batch\01_create_new_table.bat  
ECHO public_zec - script 01 done - tables created

call 00_scripts\public_zec\scripts_batch\02_truncate_append.bat  
ECHO public_zec - script 02 done - truncate_append done

call 00_scripts\public_zec\scripts_batch\03_publish.bat
ECHO public_zec - script 03 done - data published
