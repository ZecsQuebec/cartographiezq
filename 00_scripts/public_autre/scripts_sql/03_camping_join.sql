-- CAMPING
drop table if exists carto.temp;
drop table if exists carto.tr;
drop table if exists carto.copie_table;
drop table if exists carto.copie_camping;

-- JOIN 1 : CAMPING + SERVICES CAMPING
-- changer les codes numériques en leurs valeurs texte
create table corpo.temp as select * from corpo.camping_service;
alter table corpo.temp add column newcol text;
update corpo.temp 
	set newcol = corpo.service_camping.servi_camp
    from corpo.service_camping
    where corpo.temp.serv_c_id = corpo.service_camping.id_serv_c;

-- combiner les valeurs par sifz unique (garder une seule ligne par id)
create table corpo.tr 
	as select camping_id, array_to_string(array_agg(newcol order by newcol),',') as newcol 
	from corpo.temp 
    group by camping_id;
delete from corpo.tr where camping_id is null;

-- créer une nouvelle table dans le schema CARTO avec toutes les données de camping + la nouvelle colonne  
create table carto.copie_table (like corpo.camping excluding constraints); -- les constraintes sont enlevées parce que sinon Esri écrase le ID quand la variable est un  pkey et assigne des nouveaux ID
insert into carto.copie_table select * from corpo.camping;
alter table carto.copie_table add column servi_camp text; 
update carto.copie_table 	
	set servi_camp = corpo.tr.newcol 
	from corpo.tr
	where carto.copie_table.id_camping = corpo.tr.camping_id;

-- effacer les deux tables temporaires qui ne serviront plus
drop table corpo.temp, corpo.tr;

-- Après le dernier join, enlever les SIFZ qui n'existent plus  
delete from carto.copie_table where delete_flag = 1;

--Enlever les campings qui n'existe pas encore
delete from carto.copie_table where "statut_sif" = 'Projeté';
delete from carto.copie_table where "statut_sif" = 'En développement';

--changer le nom de la nouvelle table créée
alter table carto.copie_table rename to copie_camping;

