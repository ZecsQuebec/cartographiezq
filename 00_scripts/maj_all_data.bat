@ECHO OFF

cd\
cd %userprofile%
git clone https://bitbucket.org/ZecsQuebec/cartographiezq
cd cartographiezq

call 00_scripts\public_autre\scripts_batch\public_autre.bat  
ECHO public_autre mis à jour

call 00_scripts\public_chemin\scripts_batch\public_chemin.bat  
ECHO public_chemin mis à jour

call 00_scripts\public_lac\scripts_batch\public_lac.bat  
ECHO public_lac mis à jour

call 00_scripts\public_ponceau\scripts_batch\public_ponceau.bat  
ECHO public_ponceau mis à jour

call 00_scripts\public_zec\scripts_batch\public_zec.bat  
ECHO public_zec mis à jour

call 00_scripts\securise\scripts_batch\securise.bat  
ECHO securise mis à jour

cd..
rmdir /s /q cartographiezq
PAUSE
