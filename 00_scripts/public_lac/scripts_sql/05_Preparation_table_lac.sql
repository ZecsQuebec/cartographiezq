--Modifer ctype de colonne pour le objectid des lacs
------------------------------------------------------

alter table carto.copie_lac alter column "id_lac" type integer;


--supprimer la colonne the_geom et ne garder que la colonne the_geom_simpl
---------------------------------------------------------

alter table carto.copie_lac drop column "the_geom";


--préciser la projection
------------------------------------------------------
select UpdateGeometrySRID('carto', 'copie_lac', 'the_geom_simpl', 3857);

--changer nom colonne regl_partih qui est plus de 10 caractère
------------------------------------------------------
alter table carto.copie_lac rename column "regl_partih" to "reg_partih";
update carto.copie_lac set "reg_partih" = null where "reg_partih" = '' ;

--Changer le format de date parce que sinon dans ArcGIS Online il affiche l'heure et le fuseau horaire
alter table carto.copie_lac add column "date" text;
update carto.copie_lac set "date" = to_char("date_ouvh", 'DD-MM-YYYY');
alter table carto.copie_lac alter column "date_ouvh" type text;
update carto.copie_lac set "date_ouvh" = "date";
alter table carto.copie_lac drop column "date";

alter table carto.copie_lac add column "date" text;
update carto.copie_lac set "date" = to_char("date_fermh", 'DD-MM-YYYY');
alter table carto.copie_lac alter column "date_fermh" type text;
update carto.copie_lac set "date_fermh" = "date";
alter table carto.copie_lac drop column "date";


--Changer les vides pour des nulls
update carto.copie_lac set "raison_aut" = null where "raison_aut" = '' ;
update carto.copie_lac set "raison_fer" = null where "raison_fer" = '' ;
