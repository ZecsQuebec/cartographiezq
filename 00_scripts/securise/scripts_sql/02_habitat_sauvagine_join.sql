-- HABITAT SAUVAGINE
drop table if exists carto.temp;
drop table if exists carto.tr;
drop table if exists carto.copie_table;
drop table if exists carto.copie_habitat_sauvagine;


-- JOIN 1 : HABITAT SAUVAGINE + ESPÈCES SAUVAGINE
-- changer les codes numériques en leurs valeurs texte
create table corpo.temp as select * from corpo.hab_sauvagine_esp;
alter table corpo.temp add column newcol text;
update corpo.temp 
	set newcol = corpo.esp_sau.esp_sauvag
    from corpo.esp_sau
    where corpo.temp.esp_sau_id = corpo.esp_sau.id_esp_sau;

-- combiner les valeurs par sifz unique (garder une seule ligne par id)
create table corpo.tr 
	as select hab_sauv_id, array_to_string(array_agg(newcol order by newcol),',') as newcol 
	from corpo.temp 
    group by hab_sauv_id;
delete from corpo.tr where hab_sauv_id is null;

-- créer une nouvelle table dans le schema CARTO avec toutes les données de MY_TABLE + la nouvelle colonne  
create table carto.copie_table (like corpo.habitat_sauvagine excluding constraints); -- les constraintes sont enlevées parce que sinon Esri écrase le ID quand la variable est un  pkey et assigne des nouveaux ID
insert into carto.copie_table select * from corpo.habitat_sauvagine;
alter table carto.copie_table add column esp_sauvag text; 
update carto.copie_table 	
	set esp_sauvag = corpo.tr.newcol 
	from corpo.tr
	where carto.copie_table.id_sauvagi = corpo.tr.hab_sauv_id;

-- effacer les deux tables temporaires qui ne serviront plus
drop table corpo.temp, corpo.tr;

-- Après le dernier join, enlever les SIFZ qui n'existent plus et changer le nom de la nouvelle table créée
delete from carto.copie_table where delete_flag = 1;
alter table carto.copie_table rename to copie_habitat_sauvagine;