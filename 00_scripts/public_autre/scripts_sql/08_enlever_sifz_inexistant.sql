-- ENLEVER LES SIFZ INEXISTANTS (delete_flag = 1)

-- Attrait 
drop table if exists carto.copie_attrait;
create table carto.copie_attrait (like corpo.attrait excluding constraints); -- les constraintes sont enlev�es parce que sinon Esri �crase le ID quand la variable est un  pkey et assigne des nouveaux ID
insert into carto.copie_attrait select * from corpo.attrait where delete_flag IS NULL;

-- Rampe de mise � l'eau
drop table if exists carto.copie_rampe;
create table carto.copie_rampe (like corpo.rampe excluding constraints); -- les constraintes sont enlev�es parce que sinon Esri �crase le ID quand la variable est un  pkey et assigne des nouveaux ID
insert into carto.copie_rampe select * from corpo.rampe where delete_flag IS NULL;

-- Location embarcations
drop table if exists carto.copie_location;
create table carto.copie_location (like corpo.location excluding constraints); -- les constraintes sont enlev�es parce que sinon Esri �crase le ID quand la variable est un  pkey et assigne des nouveaux ID 
insert into carto.copie_location select * from corpo.location where delete_flag IS NULL;

-- Plage
drop table if exists carto.copie_plage;
create table carto.copie_plage (like corpo.plage excluding constraints); -- les constraintes sont enlev�es parce que sinon Esri �crase le ID quand la variable est un  pkey et assigne des nouveaux ID 
insert into carto.copie_plage select * from corpo.plage where delete_flag IS NULL;

-- Canot-camping
drop table if exists carto.copie_circuit_canot_camping;
create table carto.copie_circuit_canot_camping (like corpo.circuit_canot_camping excluding constraints); -- les constraintes sont enlev�es parce que sinon Esri �crase le ID quand la variable est un  pkey et assigne des nouveaux ID
insert into carto.copie_circuit_canot_camping select * from corpo.circuit_canot_camping where delete_flag IS NULL;

-- Sentier motoris�
drop table if exists carto.copie_sentier_motorise;
create table carto.copie_sentier_motorise (like corpo.sentier_motorise excluding constraints); -- les constraintes sont enlev�es parce que sinon Esri �crase le ID quand la variable est un  pkey et assigne des nouveaux ID
insert into carto.copie_sentier_motorise select * from corpo.sentier_motorise where delete_flag IS NULL;
update carto.copie_sentier_motorise set "stationnem" = null where "stationnem" = '' ;

-- Sentier r�cr�atif
drop table if exists carto.copie_sentier_recreatif;
create table carto.copie_sentier_recreatif (like corpo.sentier_recreatif excluding constraints); -- les constraintes sont enlev�es parce que sinon Esri �crase le ID quand la variable est un  pkey et assigne des nouveaux ID
insert into carto.copie_sentier_recreatif select * from corpo.sentier_recreatif where delete_flag IS NULL;
