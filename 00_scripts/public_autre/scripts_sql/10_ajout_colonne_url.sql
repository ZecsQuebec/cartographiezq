--ajouter colonne "url" pour le lien vers la page web, ces liens seront insérés dans les popups "Référez vous au site Web de la zec pour plus d'informations"

--poste d'accueil
alter table carto.copie_poste_accueil add column "url" text;
update carto.copie_poste_accueil set "url" = 'https://zec'||lower("nom_zec")||'.reseauzec.com/postes-daccueil';
update carto.copie_poste_accueil set "url" =replace("url", '-', '');
update carto.copie_poste_accueil set "url" = replace("url", ' ', '');
update carto.copie_poste_accueil set "url" = replace("url", 'é', 'e');
update carto.copie_poste_accueil set "url" = replace("url", 'è', 'e');
update carto.copie_poste_accueil set "url" = replace("url", 'postesdaccueil', 'postes-daccueil');


--camping
alter table carto.copie_camping add column "url" text;
update carto.copie_camping set "url" = 'https://zec'||lower("nom_zec")||'.reseauzec.com/hebergement-camping';
update carto.copie_camping set "url" =replace("url", '-', '');
update carto.copie_camping set "url" = replace("url", ' ', '');
update carto.copie_camping set "url" = replace("url", 'é', 'e');
update carto.copie_camping set "url" = replace("url", 'è', 'e');
update carto.copie_camping set "url" = replace("url", 'hebergementcamping', 'hebergement-camping');

--chalet
alter table carto.copie_chalet add column "url" text;
update carto.copie_chalet set "url" = 'https://zec'||lower("nom_zec")||'.reseauzec.com/hebergement-camping';
update carto.copie_chalet set "url" =replace("url", '-', '');
update carto.copie_chalet set "url" = replace("url", ' ', '');
update carto.copie_chalet set "url" = replace("url", 'é', 'e');
update carto.copie_chalet set "url" = replace("url", 'è', 'e');
update carto.copie_chalet set "url" = replace("url", 'hebergementcamping', 'hebergement-camping');