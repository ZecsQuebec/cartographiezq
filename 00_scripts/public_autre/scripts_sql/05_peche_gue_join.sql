-- PÊCHE À GUÉ
drop table if exists carto.temp;
drop table if exists carto.tr;
drop table if exists carto.copie_table;
drop table if exists carto.copie_peche_gue;

-- JOIN 1 : PÊCHE À GUÉ + ESPÈCES DE POISSON
-- changer les codes numériques en leurs valeurs texte
create table corpo.temp as select * from corpo.pech_gue_sp_pois;
alter table corpo.temp add column newcol text;
update corpo.temp 
	set newcol = corpo.espece_poisson.sp_poisson
    from corpo.espece_poisson
    where corpo.temp.sp_pois_id = corpo.espece_poisson.id_sp_pois;

-- combiner les valeurs par sifz unique (garder une seule ligne par id)
create table corpo.tr 
	as select p_gue_id, array_to_string(array_agg(newcol order by newcol),',') as newcol 
	from corpo.temp 
    group by p_gue_id;
delete from corpo.tr where p_gue_id is null;

-- créer une nouvelle table dans le schema CARTO avec toutes les données de peche_gue + la nouvelle colonne 
create table carto.copie_table (like corpo.peche_gue excluding constraints); -- les constraintes sont enlevées parce que sinon Esri écrase le ID quand la variable est un  pkey et assigne des nouveaux ID
insert into carto.copie_table select * from corpo.peche_gue;
alter table carto.copie_table add column sp_poisson text;
update carto.copie_table	
	set sp_poisson = corpo.tr.newcol
	from corpo.tr
	where carto.copie_table.id_p_gue = corpo.tr.p_gue_id;

-- effacer les deux tables temporaires qui ne serviront plus
drop table corpo.temp, corpo.tr;

-- Après le dernier join, enlever les SIFZ qui n'existent plus et changer le nom de la nouvelle table créée
delete from carto.copie_table where delete_flag = 1;
alter table carto.copie_table rename to copie_peche_gue;