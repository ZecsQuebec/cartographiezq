--Pr�paration de la couche zec pour upload sur ArcGIS online
--------------------------------------------------------
drop table if exists carto.copie_zec;
create table carto.copie_zec (like corpo.zec excluding constraints); -- les constraintes sont enlev�es parce que sinon Esri �crase le ID quand la variable est un  pkey et assigne des nouveaux ID
insert into carto.copie_zec select * from corpo.zec;
alter table carto.copie_zec drop column "geom_simple";

