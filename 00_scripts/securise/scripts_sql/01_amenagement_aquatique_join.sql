-- AMÉNAGEMENT AQUATIQUE
drop table if exists carto.temp;
drop table if exists carto.tr;
drop table if exists carto.copie_table;
drop table if exists carto.copie_amenagement_aquatique;


-- JOIN 1 : AMÉNAGEMENT AQUATIQUE + ESPÈCES POISSON
-- changer les codes numériques en leurs valeurs texte
create table corpo.temp as select * from corpo.ame_sp_pois;
alter table corpo.temp add column newcol text;
update corpo.temp 
	set newcol = corpo.espece_poisson.sp_poisson
    from corpo.espece_poisson
    where corpo.temp.sp_pois_id = corpo.espece_poisson.id_sp_pois;

-- combiner les valeurs par sifz unique (garder une seule ligne par id)
create table corpo.tr 
	as select ame_aqu_id, array_to_string(array_agg(newcol order by newcol),',') as newcol 
	from corpo.temp 
    group by ame_aqu_id;
delete from corpo.tr where ame_aqu_id is null;

-- créer une nouvelle table dans le schema CARTO avec toutes les données de amenagement_aquatique + la nouvelle colonne  
create table carto.copie_table (like corpo.amenagement_aquatique excluding constraints); -- les constraintes sont enlevées parce que sinon Esri écrase le ID quand la variable est un  pkey et assigne des nouveaux ID
insert into carto.copie_table select * from corpo.amenagement_aquatique;
alter table carto.copie_table add column sp_poisson text; 
update carto.copie_table 	
	set sp_poisson = corpo.tr.newcol 
	from corpo.tr
	where carto.copie_table.id_ame_aqu = corpo.tr.ame_aqu_id;

-- effacer les deux tables temporaires qui ne serviront plus
drop table corpo.temp, corpo.tr;


-- JOIN 2 : AMÉNAGEMENT AQUATIQUE + ESPÈCES INDÉSIRABLES
create table corpo.temp as select * from corpo.ame_sp_indesi; 
alter table corpo.temp add column newcol text;
update corpo.temp 
	set newcol = corpo.sp_indesi.sp_indesi
    from corpo.sp_indesi
    where corpo.temp.sp_ind_id = corpo.sp_indesi.id_sp_ind;
    
create table corpo.tr 
	as select ame_aqu_id, array_to_string(array_agg(newcol order by newcol),',') as newcol
	from corpo.temp 
    group by ame_aqu_id;
delete from corpo.tr where ame_aqu_id is null; 

alter table carto.copie_table add column sp_indesi_b text;
update carto.copie_table
	set sp_indesi_b = corpo.tr.newcol
	from corpo.tr
    where carto.copie_table.id_ame_aqu = corpo.tr.ame_aqu_id;

drop table corpo.temp, corpo.tr;


-- Après le dernier join, enlever les SIFZ qui n'existent plus et changer le nom de la nouvelle table créée
delete from carto.copie_table where delete_flag = 1;
alter table carto.copie_table rename to copie_amenagement_aquatique;