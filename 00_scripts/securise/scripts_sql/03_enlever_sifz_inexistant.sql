-- ENLEVER LES SIFZ INEXISTANTS (delete_flag = 1)

-- Habitat cerf
drop table if exists carto.copie_habitat_cerf;
create table carto.copie_habitat_cerf (like corpo.habitat_cerf excluding constraints); -- les constraintes sont enlev�es parce que sinon Esri �crase le ID quand la variable est un  pkey et assigne des nouveaux ID 
insert into carto.copie_habitat_cerf select * from corpo.habitat_cerf where delete_flag IS NULL;

-- Habitat orignal
drop table if exists carto.copie_habitat_orignal;
create table carto.copie_habitat_orignal (like corpo.habitat_orignal excluding constraints); -- les constraintes sont enlev�es parce que sinon Esri �crase le ID quand la variable est un  pkey et assigne des nouveaux ID
insert into carto.copie_habitat_orignal select * from corpo.habitat_orignal where delete_flag IS NULL;

-- Habitat caribou
drop table if exists carto.copie_habitat_caribou;
create table carto.copie_habitat_caribou (like corpo.habitat_caribou excluding constraints); -- les constraintes sont enlev�es parce que sinon Esri �crase le ID quand la variable est un  pkey et assigne des nouveaux ID 
insert into carto.copie_habitat_caribou select * from corpo.habitat_caribou where delete_flag IS NULL;

-- Habitat autres esp�ces
drop table if exists carto.copie_hab_autre_esp;
create table carto.copie_hab_autre_esp (like corpo.hab_autre_esp excluding constraints); -- les constraintes sont enlev�es parce que sinon Esri �crase le ID quand la variable est un  pkey et assigne des nouveaux ID
insert into carto.copie_hab_autre_esp select * from corpo.hab_autre_esp where delete_flag IS NULL;