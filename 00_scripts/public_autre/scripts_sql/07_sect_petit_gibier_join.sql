-- SECTEUR PETIT GIBIER
drop table if exists carto.temp;
drop table if exists carto.tr;
drop table if exists carto.copie_table;
drop table if exists carto.copie_sect_petit_gibier;

-- JOIN 1 : SECTEUR PETIT GIBIER + ESPÈCE PETIT GIBIER
-- changer les codes numériques en leurs valeurs texte
create table corpo.temp as select * from corpo.secteur_esp_pt_gib;
alter table corpo.temp add column newcol text;
update corpo.temp 
	set newcol = corpo.esp_pt_gib.esp_pt_gib
    from corpo.esp_pt_gib
    where corpo.temp.id_esp_pt_gib = corpo.esp_pt_gib.id_pt_gib;

-- combiner les valeurs par sifz unique (garder une seule ligne par id)
create table corpo.tr 
	as select id_secteur, array_to_string(array_agg(newcol order by newcol),',') as newcol 
	from corpo.temp 
    group by id_secteur;
delete from corpo.tr where id_secteur is null;

-- créer une nouvelle table dans le schema CARTO avec toutes les données de sect_petit_gibier + la nouvelle colonne  
create table carto.copie_table (like corpo.sect_petit_gibier excluding constraints); -- les constraintes sont enlevées parce que sinon Esri écrase le ID quand la variable est un  pkey et assigne des nouveaux ID
insert into carto.copie_table select * from corpo.sect_petit_gibier;
alter table carto.copie_table add column esp_pt_gib text; 
update carto.copie_table 	
	set esp_pt_gib = corpo.tr.newcol 
	from corpo.tr
	where carto.copie_table.id_sec_pet = corpo.tr.id_secteur;

-- effacer les deux tables temporaires qui ne serviront plus
drop table corpo.temp, corpo.tr;

-- Après le dernier join, enlever les SIFZ qui n'existent plus et changer le nom de la nouvelle table créée
delete from carto.copie_table where delete_flag = 1;
alter table carto.copie_table rename to copie_sect_petit_gibier;