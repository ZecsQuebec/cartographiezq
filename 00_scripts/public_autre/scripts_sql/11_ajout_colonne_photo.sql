--Ajouter des colonnes "photo" pour y insérer un URL vers la photo stockée en ligne pour qu'elle s'affiche ensuite dans les popups
----------------------------------------------------------------------------------------------------------------------------------

--Poste d'accueil
-----------------
alter table carto.copie_poste_accueil add column "photo" text;


--Camping
-------------
alter table carto.copie_camping add column "photo" text;

--Chalet
-------------
alter table carto.copie_chalet add column "photo" text;

--Attrait
------------
alter table carto.copie_attrait add column "photo_1" text;
alter table carto.copie_attrait add column "photo_2" text;
alter table carto.copie_attrait add column "photo_3" text;

--Rampe de mise à l'eau
-------------------------
alter table carto.copie_rampe add column "photo" text;

--Location d'embarcation
-------------------------
alter table carto.copie_location add column "photo" text;

--Pêche à gué
-----------------------
alter table carto.copie_peche_gue add column "photo" text;

--Plage
-----------------------
alter table carto.copie_plage add column "photo" text;

--Barrage
----------------------
alter table carto.copie_barrage_amenage add column "photo" text;

--Canot-camping
---------------------
alter table carto.copie_circuit_canot_camping add column "photo" text;

--Sentier motorise
--------------------
alter table carto.copie_sentier_motorise add column "photo" text;

--Sentier recreatif
--------------------
alter table carto.copie_sentier_recreatif add column "photo" text;
