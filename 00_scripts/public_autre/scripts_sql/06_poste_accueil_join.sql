-- POSTES D'ACCUEIL
drop table if exists carto.temp;
drop table if exists carto.tr;
drop table if exists carto.copie_table;
drop table if exists carto.copie_poste_accueil;

-- JOIN 1 : POSTES D'ACCUEIL + SERVICES (corpo.service)
-- changer les services aux valeurs text
create table corpo.temp as select * from corpo.poste_service; 
alter table corpo.temp add column newcol text;
update corpo.temp 
	set newcol = corpo.service.service
    from corpo.service
    where corpo.temp.service_id = corpo.service.id_service;

-- combiner les services par poste d'accueil (garder une seule ligne par id)
create table corpo.tr 
	as select poste_id, array_to_string(array_agg(newcol order by newcol),',') as newcol 
	from corpo.temp 
    group by poste_id;
delete from corpo.tr where poste_id is null;

-- créer une nouvelle table dans le schema CARTO avec toutes les données de poste_accueil + la nouvelle colonne 
create table carto.copie_table (like corpo.poste_accueil excluding constraints); -- les constraintes sont enlevées parce que sinon Esri écrase le ID quand la variable est un  pkey et assigne des nouveaux ID
insert into carto.copie_table select * from corpo.poste_accueil;
alter table carto.copie_table add column service text;
update carto.copie_table	
	set service = corpo.tr.newcol
	from corpo.tr
	where carto.copie_table.id_poste = corpo.tr.poste_id;

-- effacer les deux tables temporaires qui ne serviront plus
drop table corpo.temp, corpo.tr;



-- JOIN 2 : POSTES D'ACCUEIL + SERVICES DE LOCATION (poste_serv_loca)
create table corpo.temp as select * from corpo.poste_serv_loca;
alter table corpo.temp add column newcol text;
update corpo.temp 
	set newcol = corpo.serv_loca.serv_loca
    from corpo.serv_loca
    where corpo.temp.loca_id = corpo.serv_loca.id_loca;
    
create table corpo.tr 
	as select poste_id, array_to_string(array_agg(newcol order by newcol),',') as newcol
	from corpo.temp 
    group by poste_id;
delete from corpo.tr where poste_id is null; 

alter table carto.copie_table add column serv_loca text;
update carto.copie_table
	set serv_loca = corpo.tr.newcol
	from corpo.tr
    where carto.copie_table.id_poste = corpo.tr.poste_id;

drop table corpo.temp, corpo.tr;


-- JOIN 3 : POSTES D'ACCUEIL + SERVICES DE VENTE (poste_serv_vent)
create table corpo.temp as select * from corpo.poste_serv_vent; 
alter table corpo.temp add column newcol text; 
update corpo.temp 
	set newcol = corpo.serv_vente.serv_vente
    from corpo.serv_vente
    where corpo.temp.vente_id = corpo.serv_vente.id_vente; 
    
create table corpo.tr 
	as select poste_id, array_to_string(array_agg(newcol order by newcol),',') as newcol
	from corpo.temp 
    group by poste_id;
delete from corpo.tr where poste_id is null;

alter table carto.copie_table add column serv_vente text;
update carto.copie_table
	set serv_vente = corpo.tr.newcol
	from corpo.tr
    where carto.copie_table.id_poste = corpo.tr.poste_id;

drop table corpo.temp, corpo.tr;

-- Après le dernier join, enlever les SIFZ qui n'existent plus et changer le nom de la nouvelle table créée
delete from carto.copie_table where delete_flag = 1;

--Enlever les postes d'accueils qui appartiennent à une autre zec mais qui font de l'enregistrement pour eux
delete from carto.copie_table where sur_carto=false;

--Renommer la table;
alter table carto.copie_table rename to copie_poste_accueil;

--Enlever les colonnes qui n'ont pas besoin d'être dans les données ouvertes
alter table carto.copie_poste_accueil drop column "ty_terrain";
alter table carto.copie_poste_accueil drop column "comm_immeu";
