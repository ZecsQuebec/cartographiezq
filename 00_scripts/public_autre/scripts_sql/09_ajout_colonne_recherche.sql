--Insérer des colonnes "recherche" aux tables copies dans schéma carto. Cette colonne sera utilisé pour afficher le résultats de l'outil de recherche sur les WebApp

--poste d'accueil
alter table carto.copie_poste_accueil add column "recherche" text;
update carto.copie_poste_accueil set "recherche" = 'Poste d''accueil '||"nom_poste" ||' ('|| ("nom_zec")||')';

--camping
alter table carto.copie_camping add column "recherche" text;
update carto.copie_camping set "recherche" = 'Camping '||"nom" ||' ('|| ("nom_zec")||')';

--chalet
alter table carto.copie_chalet add column "recherche" text;
update carto.copie_chalet set "recherche" = 'Hebergement'||"nom_chalet" ||' ('|| ("nom_zec")||')';

--attrait
alter table carto.copie_attrait add column "recherche" text;
update carto.copie_attrait set "recherche" = 'Attrait '||"nom" ||' ('|| ("nom_zec")||')';

--canot-camping
alter table carto.copie_circuit_canot_camping add column "recherche" text;
update carto.copie_circuit_canot_camping set "recherche" = 'Circuit de canot-camping '||"nom" ||' ('|| ("nom_zec")||')';


--Sentier motorisé
alter table carto.copie_sentier_motorise add column "recherche" text;
update carto.copie_sentier_motorise set "recherche" = 'Sentier motorise '||"nom_sent" ||' ('|| ("nom_zec")||')';


--Sentier recreatif
alter table carto.copie_sentier_recreatif add column "recherche" text;
update carto.copie_sentier_recreatif set "recherche" = 'Sentier recreatif '||"nom_sent" ||' ('|| ("nom_zec")||')';

--Habitat petit gibier
alter table carto.copie_sect_petit_gibier add column "recherche" text;
update carto.copie_sect_petit_gibier set "recherche" = 'Secteur de petit gibier '||"nom_sect" ||' ('|| ("nom_zec")||')';

 