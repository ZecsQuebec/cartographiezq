--Créer une copie de la table lac pour avoir une couche pêche blanche, pêche à la mouche et ensemencement

--Pêche blanche
drop table if exists carto.copie_peche_blanche;
create table carto.copie_peche_blanche as select "id_lac", "nom_offici", "nom_commun", "zec_id", "nom_zec", "zone_peche", "peche_hiv", "last_user", "last_update", 
"delete_flag", "the_geom", "regl_partih", "date_ouvh", "date_fermh", "reg_comenh" from corpo.lac where "peche_hiv" = true;
alter table carto.copie_peche_blanche alter column "id_lac" type integer;

alter table carto.copie_peche_blanche add column "centroid" geometry;
alter table carto.copie_peche_blanche add column "surface" geometry;
alter table carto.copie_peche_blanche add column "intersect" boolean;
alter table carto.copie_peche_blanche add column "point" geometry;

update carto.copie_peche_blanche set "centroid" = St_Centroid("the_geom");
update carto.copie_peche_blanche set "surface" = St_PointOnSurface("the_geom");
update carto.copie_peche_blanche set "intersect" = St_Intersects(St_Centroid("the_geom"),  "the_geom");


update carto.copie_peche_blanche set "point" =
(case when "intersect" is true then  "centroid" 
else "surface" end) ;

alter table carto.copie_peche_blanche drop column "centroid";
alter table carto.copie_peche_blanche drop column "surface";
alter table carto.copie_peche_blanche drop column "intersect";
alter table carto.copie_peche_blanche drop column "the_geom";


--Pêche à la mouche
drop table if exists carto.copie_peche_mouche;
create table carto.copie_peche_mouche as select "id_lac", "nom_offici", "nom_commun", "zec_id", "nom_zec", "zone_peche", "peche_mouc", "last_user", "last_update", 
"delete_flag", "the_geom" from corpo.lac where "peche_mouc" = true;
alter table carto.copie_peche_mouche alter column "id_lac" type integer;

alter table carto.copie_peche_mouche add column "centroid" geometry;
alter table carto.copie_peche_mouche add column "surface" geometry;
alter table carto.copie_peche_mouche add column "intersect" boolean;
alter table carto.copie_peche_mouche add column "point" geometry;

update carto.copie_peche_mouche set "centroid" = St_Centroid("the_geom");
update carto.copie_peche_mouche set "surface" = St_PointOnSurface("the_geom");
update carto.copie_peche_mouche set "intersect" = St_Intersects(St_Centroid("the_geom"),  "the_geom");


update carto.copie_peche_mouche set "point" =
(case when "intersect" is true then  "centroid" 
else "surface" end) ;


alter table carto.copie_peche_mouche drop column "centroid";
alter table carto.copie_peche_mouche drop column "surface";
alter table carto.copie_peche_mouche drop column "intersect";
alter table carto.copie_peche_mouche drop column "the_geom";
select UpdateGeometrySRID('carto', 'copie_peche_mouche', 'point', 3857);


--Ensemencé
drop table if exists carto.copie_ensemence;
create table carto.copie_ensemence as select "id_lac", "nom_offici", "nom_commun", "zec_id", "nom_zec", "zone_peche", "type_ensem", "ensemence", "last_user", "last_update", 
"delete_flag", "the_geom" from corpo.lac where "ensemence" = true;
alter table carto.copie_ensemence alter column "id_lac" type integer;


alter table carto.copie_ensemence add column "centroid" geometry;
alter table carto.copie_ensemence add column "surface" geometry;
alter table carto.copie_ensemence add column "intersect" boolean;
alter table carto.copie_ensemence add column "point" geometry;

update carto.copie_ensemence set "centroid" = St_Centroid("the_geom");
update carto.copie_ensemence set "surface" = St_PointOnSurface("the_geom");
update carto.copie_ensemence set "intersect" = St_Intersects(St_Centroid("the_geom"),  "the_geom");


update carto.copie_ensemence set "point" =
(case when "intersect" is true then  "centroid" 
else "surface" end) ;


alter table carto.copie_ensemence drop column "centroid";
alter table carto.copie_ensemence drop column "surface";
alter table carto.copie_ensemence drop column "intersect";
alter table carto.copie_ensemence drop column "the_geom";
select UpdateGeometrySRID('carto', 'copie_ensemence', 'point', 3857);





 -------------------------------------------------------------------
 -- Bouger point qui se superposent
 -------------------------------------------------------------------
 --Pêche blanche
 --bouger point qui intersect peche à la mouche et ensemencement
--
 alter table carto.copie_peche_blanche add column "mouche" geometry;
 alter table carto.copie_peche_blanche add column "ensemence" geometry;
 alter table carto.copie_peche_blanche add column "intersect_m" boolean;
  alter table carto.copie_peche_blanche add column "intersect_e" boolean;
 alter table carto.copie_peche_blanche add column "geom" geometry;


--trouver les lac avec des données aussi de peche à la mouche et ensemence et trouvé si les points sont exactements au même endroit
select UpdateGeometrySRID('carto', 'copie_peche_blanche', 'point', 3857);
update carto.copie_peche_blanche set "mouche" = copie_peche_mouche.point from carto.copie_peche_mouche where copie_peche_mouche.id_lac = copie_peche_blanche.id_lac;
update carto.copie_peche_blanche set "ensemence" = copie_ensemence.point from carto.copie_ensemence where copie_ensemence.id_lac = copie_peche_blanche.id_lac;


update carto.copie_peche_blanche set "intersect_m" = St_Intersects("point", "mouche");
update carto.copie_peche_blanche set "intersect_e" = St_Intersects("point", "ensemence");

--déplacer les points qui se superposent
  update carto.copie_peche_blanche set "geom" = 
  (case when "intersect_m" is true then St_Translate("point", 0,70)
  when "intersect_e" is true then St_Translate("point", -55,70)
  else St_Translate("point",0,80) end);


 
--Pêche à la mouche
--bouger point qui intersect peche à la mouche et ensemencement
--
 alter table carto.copie_peche_mouche add column "blanche" geometry;
 alter table carto.copie_peche_mouche add column "ensemence" geometry;
 alter table carto.copie_peche_mouche add column "intersect_b" boolean;
  alter table carto.copie_peche_mouche add column "intersect_e" boolean;
 alter table carto.copie_peche_mouche add column "geom" geometry;


--trouver les lac avec des données aussi de peche à la mouche et ensemence et trouvé si les points sont exactements au même endroit
select UpdateGeometrySRID('carto', 'copie_peche_mouche', 'point', 3857);
update carto.copie_peche_mouche set "blanche" = copie_peche_blanche.point from carto.copie_peche_blanche where copie_peche_blanche.id_lac = carto.copie_peche_mouche.id_lac;
update carto.copie_peche_mouche set "ensemence" = copie_ensemence.point from carto.copie_ensemence where copie_ensemence.id_lac = carto.copie_peche_mouche.id_lac;


update carto.copie_peche_mouche set "intersect_b" = St_Intersects("point", "blanche");
update carto.copie_peche_mouche set "intersect_e" = St_Intersects("point", "ensemence");

--déplacer les points qui se superposent
  update carto.copie_peche_mouche set "geom" = 
  (case when "intersect_b" is true then St_Translate("point", -110,70)
  when "intersect_e" is true then St_Translate("point", -55,70)
  else St_Translate("point",0,80) end);





--Ensemencé
--bouger point qui intersect peche à la mouche et ensemencement
--
 alter table carto.copie_ensemence add column "blanche" geometry;
 alter table carto.copie_ensemence add column "mouche" geometry;
 alter table carto.copie_ensemence add column "intersect_b" boolean;
  alter table carto.copie_ensemence add column "intersect_m" boolean;
 alter table carto.copie_ensemence add column "geom" geometry;


--trouver les lac avec des données aussi de peche à la mouche et ensemence et trouvé si les points sont exactements au même endroit
select UpdateGeometrySRID('carto', 'copie_ensemence', 'point', 3857);
update carto.copie_ensemence set "blanche" = copie_peche_blanche.point from carto.copie_peche_blanche where copie_peche_blanche.id_lac = carto.copie_ensemence.id_lac;
update carto.copie_ensemence set "mouche" = copie_peche_mouche.point from carto.copie_peche_mouche where copie_peche_mouche.id_lac = carto.copie_ensemence.id_lac;


update carto.copie_ensemence set "intersect_b" = St_Intersects("point", "blanche");
update carto.copie_ensemence set "intersect_m" = St_Intersects("point", "mouche");

--déplacer les points qui se superposent
  update carto.copie_ensemence set "geom" = 
  (case when "intersect_b" is true then St_Translate("point", 55,70)
  when "intersect_m" is true then St_Translate("point", 55,70)
  else St_Translate("point",0,80) end);



 -------------
 --Efface les colonnes créer pour utilisation temporaire
 alter table carto.copie_peche_blanche drop column "mouche";
 alter table carto.copie_peche_blanche drop column "ensemence";
 alter table carto.copie_peche_blanche drop column "intersect_m";
 alter table carto.copie_peche_blanche drop column "intersect_e";
 alter table carto.copie_peche_blanche drop column "point";
 select UpdateGeometrySRID('carto', 'copie_peche_blanche', 'geom', 3857);

 alter table carto.copie_peche_mouche drop column "blanche";
 alter table carto.copie_peche_mouche drop column "ensemence";
 alter table carto.copie_peche_mouche drop column "intersect_b";
 alter table carto.copie_peche_mouche drop column "intersect_e";
 alter table carto.copie_peche_mouche drop column "point";
 select UpdateGeometrySRID('carto', 'copie_peche_mouche', 'geom', 3857);
 
 alter table carto.copie_ensemence drop column "blanche";
 alter table carto.copie_ensemence drop column "mouche";
 alter table carto.copie_ensemence drop column "intersect_b";
 alter table carto.copie_ensemence drop column "intersect_m";
 alter table carto.copie_ensemence drop column "point";
 select UpdateGeometrySRID('carto', 'copie_ensemence', 'geom', 3857);
