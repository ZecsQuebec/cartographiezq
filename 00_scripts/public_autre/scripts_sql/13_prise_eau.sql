--Prise d'eau
drop table if exists carto.temp;
drop table if exists carto.tr;
drop table if exists carto.copie_table;
drop table if exists carto.copie_prise_eau;

--Cr�er la table et enlever les SIFZ inexistant
drop table if exists carto.prise_eau;
create table carto.copie_prise_eau (like corpo.prise_eau excluding constraints); -- les constraintes sont enlev�es parce que sinon Esri �crase le ID quand la variable est un  pkey et assigne des nouveaux ID 
insert into carto.copie_prise_eau select * from corpo.prise_eau where delete_flag IS NULL;


--Changer le format des colonnes dates
alter table carto.copie_prise_eau add column "date" text;
update carto.copie_prise_eau set "date" = to_char("date_inv", 'DD-MM-YYYY');
alter table carto.copie_prise_eau alter column "date_inv" type text;
update carto.copie_prise_eau set "date_inv" = "date";
alter table carto.copie_prise_eau drop column "date";


alter table carto.copie_prise_eau add column "date" text;
update carto.copie_prise_eau set "date" = to_char("date_test", 'DD-MM-YYYY');
alter table carto.copie_prise_eau alter column "date_test" type text;
update carto.copie_prise_eau set "date_test" = "date";
alter table carto.copie_prise_eau drop column "date";


--pr�ciser la projection
select UpdateGeometrySRID('carto', 'copie_prise_eau', 'the_geom', 3857);
