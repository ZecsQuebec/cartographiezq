-- CHALET
drop table if exists carto.temp;
drop table if exists carto.tr;
drop table if exists carto.copie_table;
drop table if exists carto.copie_chalet;

-- JOIN 1 : CHALET + SERVICE_PR
-- changer les codes numériques en leurs valeurs texte
create table corpo.temp as select * from corpo.chalet_serv_pr;
alter table corpo.temp add column newcol text;
update corpo.temp 
	set newcol = corpo.service_pr.service_pr
    from corpo.service_pr
    where corpo.temp.serv_pr_id = corpo.service_pr.id_serv_pr;

-- combiner les valeurs par sifz unique (garder une seule ligne par id)
create table corpo.tr 
	as select chalet_id, array_to_string(array_agg(newcol order by newcol),',') as newcol 
	from corpo.temp 
    group by chalet_id;
delete from corpo.tr where chalet_id is null;

-- créer une nouvelle table dans le schema CARTO avec toutes les données de chalet + la nouvelle colonne 
create table carto.copie_table (like corpo.chalet excluding constraints); -- les constraintes sont enlevées parce que sinon Esri écrase le ID quand la variable est un  pkey et assigne des nouveaux ID
insert into carto.copie_table select * from corpo.chalet;
alter table carto.copie_table add column service_pr text;
update carto.copie_table	
	set service_pr = corpo.tr.newcol
	from corpo.tr
	where carto.copie_table.id_chalet = corpo.tr.chalet_id;

-- effacer les deux tables temporaires qui ne serviront plus
drop table corpo.temp, corpo.tr;


-- JOIN 2 : CHALET + SERVICE_SE
create table corpo.temp as select * from corpo.chalet_serv_se; 
alter table corpo.temp add column newcol text;
update corpo.temp 
	set newcol = corpo.service_se.service_se
    from corpo.service_se
    where corpo.temp.serv_se_id = corpo.service_se.id_serv_se;
    
create table corpo.tr 
	as select chalet_id, array_to_string(array_agg(newcol order by newcol),',') as newcol
	from corpo.temp 
    group by chalet_id;
delete from corpo.tr where chalet_id is null; 

alter table carto.copie_table add column service_se text;
update carto.copie_table
	set service_se = corpo.tr.newcol
	from corpo.tr
    where carto.copie_table.id_chalet = corpo.tr.chalet_id;

drop table corpo.temp, corpo.tr;

-- Après le dernier join, enlever les SIFZ qui n'existent plus et changer le nom de la nouvelle table créée
delete from carto.copie_table where delete_flag = 1; 
alter table carto.copie_table rename to copie_chalet;
