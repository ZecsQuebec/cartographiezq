-- Préparation des données pour mise en ligne sur ArcGIS Online --
------------------------------------------------------------------


------------------------------------------------------------------
--Corriger les noms d'entités
------------------------------------------------------------------
-- Dans les popups, automatiquement le type d'entité (ex. Camping) est insérer avant le nom de l'entité, quand Camping est déjà dans le titre
-- le mot est répété deux fois, code pour nettoyer le nom des entités:

-- Enlever 'Camping' lorsque la colonne "nom" commence par 'Camping' 

update corpo.camping set "nom"=replace("nom",'Camping ','');
update corpo.camping set "nom"=replace("nom",'camping ','');
--update corpo.camping set "nom"=replace("nom",'Canot-', 'Canot-camping');
update corpo.camping set "nom"=replace("nom", '  ', ' ');

-- Enlever 'Poste d'accueil' lorsque la colonne "nom_poste" commence par 'Poste d'accueil'

update corpo.poste_accueil set "nom_poste"=replace("nom_poste", 'Poste d''accueil ', '');
update corpo.poste_accueil set "nom_poste" = replace("nom_poste", 'Accueil ', '');
update corpo.poste_accueil set "nom_poste" = replace("nom_poste", '  ', ' ');

--Chalet
update corpo.chalet set "nom_chalet" = replace("nom_chalet", 'Hébergement ', '');


--Canot-camping
update corpo.circuit_canot_camping set "nom" = replace("nom", 'Circuit de canot-camping ', '');
update corpo.circuit_canot_camping set "nom" = replace("nom", 'Circuit ', '');
update corpo.circuit_canot_camping set "nom" = replace("nom", 'Circuit de canot-Camping ', '');
update corpo.circuit_canot_camping set "nom" = replace("nom", 'Circuit de kayak-camping ', '');
update corpo.circuit_canot_camping set "nom" = replace("nom", 'Parcours aménagé de canot-camping ', '');
update corpo.circuit_canot_camping set "nom" = replace("nom", 'Parcours aménagé de canot-camping ', '');

--Sentier motorisé
update corpo.sentier_motorise set "nom_sent" = replace("nom_sent", 'Sentier VTT ', '');
update corpo.sentier_motorise set "nom_sent" = replace("nom_sent", 'Sentier ', '');


--Sentier récréatif
update corpo.sentier_recreatif set "nom_sent"=replace("nom_sent", 'Sentier ', '');

