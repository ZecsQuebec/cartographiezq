# Mise � jour cartographie

Contenu:  
1. Organisation des fichiers  
2. Premi�re utilisation sur un nouvel ordinateur Windows - �tapes pr�liminaires  
3. Mise � jour des donn�es cartographiques sur la carte en ligne  


## 1. Organisation des fichiers ##

* 00_scripts contient tous les scripts n�cessaires  
	* Les scripts sont s�par�s par groupe de donn�es (correspondants aux "feature service" dans ArcGIS), chacun des groupes ayant potentiellement un fr�quence de mise � jour diff�rente.  
    * Chaque groupe contient:  
            * Les scripts_batch qui sont les scripts � rouler dans l'invite de commande (Windows), s�par�s par �tape ou en bloc.  
	        * Les scripts_sql qui sont les fichiers de codes en langage SQL qui seront  ex�cut�s dans PostGIS.	
	        * Les scripts_python qui sont les fichiers de codes en langage Python qui seront ex�cut�s dans ArcGIS Pro.  

* info_files contient des fichiers suppl�mentaires n�cessaires pour rouler les scripts.  



## 2. Premi�re utilisation sur un nouvel ordinateur Windows - �tapes pr�liminaires ##

Les scripts font appel � l'outil **psql.exe** de PostGIS et � l'outil **python.exe** de ArcGIS Pro qui doivent �tre localis�s sur l'ordinateur. Le chemin vers ces outils doit �tre export� dans la liste des PATHS du syst�me pour la suite des �tapes.  

1. Ouvrir l'Invite de commande (Windows) en tant qu'administrateur (clic droit).  

2. Trouver l'outil:  

        cd\  
        dir psql.exe /s  
    
    Dans le r�sultat de la recherche, trouver celui qui se situe dans les dossiers du logiciel.      
    *Exemple:  R�pertoire de C:\Program Files\PostgreSQL\9.5\bin*  

3. Exporter le chemin de l'outil dans le r�pertoire de l'ordinateur:  

        setx PATH "%PATH%;<chemin_de_l_outil>" /M  
    
    *Exemple pour l'outil psql:  
    `setx PATH "%PATH%;C:\Program Files\PostgreSQL\9.5\bin; C:\Program Files\PostgreSQL\9.5\lib" /M`  
    ou `setx PATH "%PATH%;%ProgramFiles%\PostgreSQL\9.5\bin ;%ProgramFiles%\PostgreSQL\9.5\lib" /M`*  

4. R�p�ter les �tapes 2 et 3 pour exporter les PATH de **python.exe**. Cet outil doit �tre celui situ� dans les dossiers d'ArcGIS Pro.  
    *Exemple: `setx PATH "%PATH%;%ProgramFiles%\ArcGIS\Pro\bin\Python\envs\arcgispro-py3" /M`*  

5. Fermer l'invite de commandes et red�marrer l'ordinateur.  

*NOTE: Faire un clic droit pour "Coller" du code dans l'invite de commande.*  



## 3. Mise � jour des donn�es cartographiques sur la carte en ligne ##

1. Ouvrir l'invite de commandes  

2. Importer le dossier de Bitbucket sur l'ordinateur:  

        git clone https://bitbucket.org/ZecsQuebec/cartographiezq  
        cd cartographiezq  

3. Faire la mise � jour des donn�es par groupe:  

       * public autre  

            call 00_scripts\public_autre\scripts_batch\public_autre.bat  
        
       * public chemin  

            call 00_scripts\public_chemin\scripts_batch\public_chemin.bat  
        
       * public lac  

            call 00_scripts\public_lac\scripts_batch\public_lac.bat  
        
       * public ponceau  

            call 00_scripts\public_ponceau\scripts_batch\public_ponceau.bat  
        
       * public zec  

            call 00_scripts\public_zec\scripts_batch\public_zec.bat  
                
       * s�curis�  

            call 00_scripts\securise\scripts_batch\securise.bat  

4. Quitter en supprimant le dossier de l'ordinateur:  

        cd..  
        rmdir /s /q cartographiezq  

 