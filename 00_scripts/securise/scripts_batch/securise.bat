@ECHO OFF
call 00_scripts\securise\scripts_batch\01_create_new_table.bat  
ECHO securise - script 01 done - tables created

call 00_scripts\securise\scripts_batch\02_truncate_append.bat  
ECHO securise - script 02 done - truncate_append done

call 00_scripts\securise\scripts_batch\03_publish.bat
ECHO securise - script 03 done - data published
