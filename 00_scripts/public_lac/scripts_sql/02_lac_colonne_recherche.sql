--Insérer des colonnes "recherche" aux tables copies dans schéma carto. Cette colonne sera utilisé pour afficher le résultats de l'outil de recherche sur les WebApp

--lac
alter table carto.copie_lac add column "recherche" text;
update carto.copie_lac set "recherche" = case when "nom_offici" is not null then "nom_offici" ||' ('|| ("nom_zec")||')' else "nom_commun" ||' ('|| ("nom_zec")||')' end ;