--Ajouter des colonnes "photo" pour y insérer un URL vers la photo stockée en ligne pour qu'elle s'affiche ensuite dans les popups
----------------------------------------------------------------------------------------------------------------------------------

--Aménagement aquatique
--------------------
alter table carto.copie_amenagement_aquatique add column "photo_1" text;
alter table carto.copie_amenagement_aquatique add column "photo_2" text;

--Habitats
---------------------
alter table carto.copie_hab_autre_esp add column "photo" text;
alter table carto.copie_habitat_caribou add column "photo" text;
alter table carto.copie_habitat_cerf add column "photo" text;
alter table carto.copie_habitat_orignal add column "photo" text;
alter table carto.copie_habitat_sauvagine add column "photo" text;