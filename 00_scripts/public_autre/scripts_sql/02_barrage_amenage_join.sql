-- BARRAGE AMÉNAGÉ
drop table if exists carto.temp;
drop table if exists carto.tr;
drop table if exists carto.copie_table;
drop table if exists carto.copie_barrage_amenage;

-- JOIN 1 : BARRAGES + ESPÈCES INDÉSIRABLES
-- changer les codes numériques en leurs valeurs texte
create table corpo.temp as select * from corpo.barrage_sp_indesi;
alter table corpo.temp add column newcol text;
update corpo.temp 
	set newcol = corpo.sp_indesi.sp_indesi
    from corpo.sp_indesi
    where corpo.temp.sp_ind_id = corpo.sp_indesi.id_sp_ind;

-- combiner les valeurs par sifz unique (garder une seule ligne par id)
create table corpo.tr 
	as select barrage_id, array_to_string(array_agg(newcol order by newcol),',') as newcol 
	from corpo.temp 
    group by barrage_id;
delete from corpo.tr where barrage_id is null;

-- créer une nouvelle table dans le schema CARTO avec toutes les données de barrage_amenage + la nouvelle colonne 
create table carto.copie_table (like corpo.barrage_amenage excluding constraints); -- les constraintes sont enlevées parce que sinon Esri écrase le ID quand la variable est un  pkey et assigne des nouveaux ID
insert into carto.copie_table select * from corpo.barrage_amenage;
alter table carto.copie_table add column sp_indesi text;
update carto.copie_table	
	set sp_indesi = corpo.tr.newcol
	from corpo.tr
	where carto.copie_table.id_barrage = corpo.tr.barrage_id;

-- effacer les deux tables temporaires qui ne serviront plus
drop table corpo.temp, corpo.tr;


-- Après le dernier join, enlever les SIFZ qui n'existent plus et changer le nom de la nouvelle table créée
delete from carto.copie_table where delete_flag = 1;
alter table carto.copie_table rename to copie_barrage_amenage;


--Changer le format de date parce que sinon dans ArcGIS Online il affiche l'heure et le fuseau horaire
alter table carto.copie_barrage_amenage add column "date" text;
update carto.copie_barrage_amenage set "date" = to_char("date_reali", 'DD-MM-YYYY');
alter table carto.copie_barrage_amenage alter column "date_reali" type text;
update carto.copie_barrage_amenage set "date_reali" = "date";
alter table carto.copie_barrage_amenage drop column "date";
