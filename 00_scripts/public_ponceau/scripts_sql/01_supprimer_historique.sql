--Garder que les derni�res donn�es de ponceaux (supprimer l'historique)
drop table if exists carto.copie_ponceau;
create table carto.copie_ponceau (like corpo.ponceau excluding constraints); -- les constraintes sont enlev�es parce que sinon Esri �crase le ID quand la variable est un  pkey et assigne des nouveaux ID
insert into carto.copie_ponceau select * from corpo.ponceau where "last_update" is null;

--changer nom de la variable objectid parce qu'Esri en cr�e une automatiquement et donc renomme notre objectid � "objectid_1"
alter table carto.copie_ponceau rename column "objectid" to "id_ponceau";